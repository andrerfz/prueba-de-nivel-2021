<?php
namespace App\Traits;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait ORMMethods {

    /**
     * @param $entity
     * @param EntityManagerInterface $em
     * @return bool
     */
    public function getStatus($entity, EntityManagerInterface $em): bool
    {
        return UnitOfWork::STATE_MANAGED === $em->getUnitOfWork()->getEntityState($entity);
    }

    public function parseResponseJson(array $data = [], int $status = 200): Response
    {
        return $this->json($data, $status)
            ->setCharset('UTF-8')
            ->setEncodingOptions(JSON_PRETTY_PRINT);
    }

    function parseValidation(ValidatorInterface $validator, $entity): ?Response
    {
        $errors = $validator->validate($entity);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }
        return null;
    }
}