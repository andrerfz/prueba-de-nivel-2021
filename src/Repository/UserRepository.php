<?php

namespace App\Repository;

use App\Entity\User;
use App\Traits\ORMMethods;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{

    use ORMMethods;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function filterSoftDelete(): QueryBuilder
    {
        return $this->createQueryBuilder('u')->where('u.deletedAt is null');
    }

    public function store(User $entity, bool $flush = true): bool
    {

        $this->_em->persist($entity);

        if ($flush) {
            $this->_em->flush();
        }

        return $this->getStatus($entity, $this->_em);
    }

    public function remove(User $entity, bool $flush = true): bool
    {

        $this->_em->remove($entity);

        if ($flush) {
            $this->_em->flush();
        }

        return $this->getStatus($entity, $this->_em);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }


}
