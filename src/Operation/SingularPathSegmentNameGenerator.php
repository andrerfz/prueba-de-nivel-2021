<?php

namespace App\Operation;

use ApiPlatform\Core\Operation\PathSegmentNameGeneratorInterface;

class SingularPathSegmentNameGenerator implements PathSegmentNameGeneratorInterface
{
    /**
     * Transforms a given string to a valid path name which can be pluralized (eg. for collections).
     *
     * @param string $name usually a ResourceMetadata shortname
     *
     * @return string A string that is a part of the route name
     */
    public function getSegmentName(string $name, bool $collection = true): string
    {
        $name = strtolower($this->despluralize($name));

        return $name;
    }

    private function despluralize(string $string): string
    {
        return substr($string, -1, 1) === 's' ? substr_replace($string ,"", -1) : $string;
    }
}