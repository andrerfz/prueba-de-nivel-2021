<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User;
use App\Entity\WorkEntry;
use App\Repository\UserRepository;
use App\Repository\WorkEntryRepository;
use App\Traits\ORMMethods;
use DateTime;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class UserController extends AbstractController
{

    use ORMMethods;

    protected UserRepository $userRepository;
    protected WorkEntryRepository $workEntryRepository;
    protected DateTime $now;

    public function __construct(UserRepository $userRepository, WorkEntryRepository $workEntryRepository)
    {
        $this->userRepository = $userRepository;
        $this->workEntryRepository = $workEntryRepository;
        $this->now = new DateTime('now');
    }

    public function index(Connection $connection): void
    {
        return;
    }

    #[Route('/api/user/{id}', name: 'app_api_user_show', methods: 'get')]
    public function show(int $id): Response
    {
        $user = $this->userRepository->find($id);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        return $this->json(['data' => $user]);
    }

    #[Route('/api/user', name: 'app_api_users_list', methods: 'get')]
    public function list(Request $request): Response
    {
        $users = $this->userRepository->filterSoftDelete()->getQuery()->getResult();

        return $this->json(['data'=> $users]);
    }

    #[Route('/api/user', name: 'app_api_user_create', methods: 'post')]
    public function create(Request $request, ValidatorInterface $validator): Response
    {
        $email = $this->userRepository->findOneBy(['email' => $request->get('email')]);

        if ($email) {
            return $this->json('Email not available!', '404');
        }
        if (!$request->get('name') || !$request->get('email')) {
            return $this->json('The field name is mandatory!', '404');
        }
        $user = new User();
        $user->setName($request->get('name'));
        $user->setEmail($request->get('email'));
        $user->setCreatedAt($this->now);
        $user->setUpdatedAt($this->now);

        $errors = $this->parseValidation($validator, $user);
        if ($errors) {
            return $errors;
        }

        $status = $this->userRepository->store($user);

        return $this->parseResponseJson(['data'=> $user], $status ? '201' : '500');
    }

    #[Route('/api/user/{id}', name: 'app_api_user_update', methods: 'put')]
    public function update(int $id, Request $request, ValidatorInterface $validator): Response
    {
        $user = $this->userRepository->find($id);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        if ($request->get('name')) {
            $user->setName($request->get('name'));
        }
        if ($request->get('email')) {
            $user->setEmail($request->get('email'));
        }
        $user->setLastName($request->get('last_name'));
        $user->setUpdatedAt($this->now);

        $errors = $this->parseValidation($validator, $user);
        if ($errors) {
            return $errors;
        }

        $status = $this->userRepository->store($user);

        return $this->json(['data'=> $user], $status ? '200' : '500');
    }

    #[Route('/api/user/{id}', name: 'app_api_user_delete', methods: 'delete')]
    public function destroy(int $id, Request $request, ValidatorInterface $validator): Response
    {
        $user = $this->userRepository->find($id);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        $user->setDeletedAt($this->now);
        $this->userRepository->store($user);

        $status = $this->userRepository->store($user);

        return $this->json('Resource deleted!', $status ? '200' : '500');
    }

    #[Route('/api/user/{userId}/work_entry', name: 'app_api_user_work_entry_list', methods: 'get')]
    public function listWorkEntries(int $userId, Request $request): Response
    {
        $user = $this->userRepository->find($userId);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        return $this->json(['data' => $user->getWorkEntries()]);
    }

    #[Route('/api/user/{userId}/work_entry', name: 'app_api_user_work_entry_create', methods: 'post')]
    public function createWorEntry(int $userId, Request $request): Response
    {
        $user = $this->userRepository->find($userId);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        $new_entry = new WorkEntry();
        $new_entry->setStartDate($request->get('startDate')
            ? $this->now->modify($request->get('startDate'))
            : $this->now
        );
        $new_entry->setCreatedAt($this->now);
        $new_entry->setUpdatedAt($this->now);
        $new_entry->setUser($user);

        $status = $this->workEntryRepository->store($new_entry);

        return $this->parseResponseJson(['data'=> $new_entry], $status ? '201' : '500');
    }

    #[Route('/api/user/{userId}/work_entry/{id}', name: 'app_api_user_work_entry_update', methods: 'put')]
    public function updateWorEntry(int $userId, int $id, Request $request): Response
    {
        $user = $this->userRepository->find($userId);
        $entry = $this->workEntryRepository->find($id);

        if ((!$user || $user->isDeleted()) || (!$entry || $entry->isDeleted())) {
            return $this->json('Resource not found!', '404');
        }

        if ($request->get('startDate')) {
            $starDate = $this->getDate($request->get('startDate'));
            if ($starDate) {
                $entry->setStartDate($starDate);
            }
        }

        if ($request->get('endDate')) {
            $starDate = $this->getDate($request->get('endDate'));
            if ($starDate) {
                $entry->setEndDate($starDate);
            }
        }

        $entry->setUpdatedAt($this->now);

        $status = $this->workEntryRepository->store($entry);

        return $this->parseResponseJson(['data'=> $entry], $status ? '200' : '500');
    }

    #[Route('/api/user/{userId}/work_entry/{id}', name: 'app_api_user_work_entry_destroy', methods: 'delete')]
    public function destroyWorEntry(int $userId, int $id): Response
    {
        $user = $this->userRepository->find($userId);
        $entry = $this->workEntryRepository->find($id);

        if ((!$user || $user->isDeleted()) || (!$entry || $entry->isDeleted())) {
            return $this->json('Resource not found!', '404');
        }

        $entry->setDeletedAt($this->now);

        $status =  $this->workEntryRepository->store($entry);

        return $this->json('Resource deleted!', $status ? '200' : '500');
    }

    /**
     * @param string $date
     * @return DateTime|null
     */
    public function getDate(string $date): ?DateTime
    {
        $starDate = null;
        try {
            $starDate = new DateTime($date);
        } catch (Throwable $e) {
        }
        return $starDate;
    }

}
