<?php

namespace App\Controller\Api;

use App\Repository\UserRepository;
use App\Repository\WorkEntryRepository;
use App\Traits\ORMMethods;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WorkEntryController extends AbstractController
{
    use ORMMethods;

    protected UserRepository $userRepository;
    protected WorkEntryRepository $workEntryRepository;
    protected DateTime $now;

    public function __construct(UserRepository $userRepository, WorkEntryRepository $workEntryRepository)
    {
        $this->userRepository = $userRepository;
        $this->workEntryRepository = $workEntryRepository;
        $this->now = new DateTime('now');
    }

    public function index(): void
    {
        return;
    }

    #[Route('/api/work_entry/{id}', name: 'app_api_work_entry_list', methods: 'get')]
    public function show(int $id, Request $request): Response
    {
        $user = $this->workEntryRepository->find($id);

        if (!$user || $user->isDeleted()) {
            return $this->json('Resource not found!', '404');
        }

        return $this->json(['data' => $user]);
    }

    #[Route('/api/work_entry', name: 'app_api_work_entries_list', methods: 'get')]
    public function list(Request $request): Response
    {
        $users = $this->workEntryRepository->filterSoftDelete()->getQuery()->getResult();

        return $this->json(['data'=> $users]);
    }
}
